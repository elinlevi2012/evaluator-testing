#! /usr/bin/python3.6
import os
import json
import pathlib
from pinger import disconnected_check
from datetime import datetime, timedelta
import time

folder = os.environ.get('folder') 
cluster_name = os.environ.get('cluster_name')
base_domain = os.environ.get('base_domain')
worker_nodes = os.environ.get('worker_nodes')
master_nodes= os.environ.get('master_nodes')

def missing_test(data):
  test_name = data["name"]
  status = "missing"
  msg = "No log from test"
  start_at = data["started_at"]
  finished_at = data["finished_at"]
  
  test_event = {
          "name" : test_name,
          "status": status,
          "msg": msg,
          "started_at": start_at,
          "finished_at ": finished_at
          }

  return test_event

def regular_event(data):
  test_name = data["name"]
  status = data["status"]
  msg = data["msg"]
  start_at = data["started_at"]
  finished_at = data["finished_at"]

  test_event = {
          "name" : test_name,
          "status": status,
          "msg": msg,
          "started_at": start_at,
          "finished_at ": finished_at
          }

  return test_event
 
def splunk_event(all_results, status ):

  final_event = {
          "sourcetype": "mgmt-tester-evaluator",
          "event": {

              "name": cluster_name,
              "status": status,
              "ocp_version": "4.3.22",
              "tests": all_results
              }
         }

  final_splunk_event = json.dumps(final_event)
  return final_splunk_event

def cluster_nodes_names():
  for x in range(int(master_nodes)):
      num = x+1
      node = "control-plane-{}.{}.{}".format(num,cluster_name,base_domain)
      cluster_nodes.append(node)

  for x in range(int(worker_nodes)):
      num = x+1
      node = "worker-{}.{}.{}".format(num,cluster_name,base_domain)
      cluster_nodes.append(node)
  return cluster_nodes

# Function that determines the status 
def calc_status(dict):
  # Checks if the Cluster is diconnected
  cluster_nodes = cluster_nodes_names()
  #if disconnected_check.is_disconnected(cluster_nodes) == True:
  if disconnected_check.is_disconnected(["localhost","127.0.0.1"]) == True:
      return "disconnected"

  # Checks if the Cluster is dead
  for filename in os.listdir(folder):
      filepath = os.path.join(folder, filename)
      with open(filepath, 'r') as json_file:
          data = json.loads(json_file.read())
      started = data["started_at"]
      started_timestamp = datetime.strptime(started, '%Y-%m-%d %H:%M:%S.%f')
      later = started_timestamp + timedelta(minutes=3)
      if later >=  datetime.now():
          entering_results.append(data)
      else:
          missing_alert = missing_test(data)
          missing_results.append(missing_alert)

  if not entering_results:
      return "dead"

  # Checks if the Cluster is sick
  for test in dict:
      filepath = os.path.join(folder, test)
      with open(filepath, 'r') as json_file:
          data = json.loads(json_file.read())
      if data["status"].find("success") == -1:
          return "sick"

  # If everything below doesn't work its alive
  return "alive"
   
def main():
  # Now get all the statuses
  for file in os.listdir(folder):
      filepath = os.path.join(folder, file)
      with open(filepath, 'r') as f:
          data = f.read()
          tests[file]= data
          f.close()

  status = calc_status(tests)
  all_results = entering_results + missing_results

  event = splunk_event(all_results, status )
  print(event)

if __name__ == "__main__":
  while True:
      cluster_nodes=[]
      tests={}
      all_results={}
      entering_results=[]
      missing_results=[]
      
      main()
      time.sleep(60)
      print("-------------------------------------------------------")

  
