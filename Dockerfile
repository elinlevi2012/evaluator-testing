FROM python:3

WORKDIR /usr/evaluator

COPY requirements.txt ./
RUN pip install -r requirements.txt
#RUN pip download -r requirements.txt

COPY . .

#CMD [ "python", "./sleep.py" ]
CMD [ "python", "./evaluate.py" ]
